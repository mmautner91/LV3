# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 19:50:14 2015

@author: bec
"""
#
#Napisite programski kod koji će iscrtati sljedeće slike:
#1. Pomoću barplot-a prikazite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
#2. Pomocu boxplot-a prikazite na istoj slici distribuciju tezine automobila s 4, 6 i 8 cilindara.
#3. Pomocu odgovarajuceg grafa pokusajte odgovoriti na pitanje imaju li automobili s rucnim mjenjacem vecu
#potrosnju od automobila s automatskim mjenjacem?
#4. Prikazite na istoj slici odnos ubrzanja i snage automobila za automobile s rucnim odnosno automatskim
#mjenjacem.


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
'''
#1. Pomoću barplot-a prikazite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
'''
mtcars = pd.read_csv('mtcars.csv')
v8=mtcars[(mtcars.cyl == 8)];
v6=mtcars[(mtcars.cyl == 6)]
b4=mtcars[(mtcars.cyl == 4)]# sa 4 cilindra

#Definition : DataFrame(data=None, index=None, columns=None, dtype=None, copy=False)
#

df=pd.DataFrame({'v8': v8.mpg, 'v6': v6.mpg , 'b4':b4.mpg}, columns=['v8', 'v6','b4'])
plt.figure()
df.plot(kind='bar')
plt.title("potrosnja automobila s 4, 6 i 8 cilindara.")

'''
#2. Pomocu boxplot-a prikazite na istoj slici distribuciju tezine automobila s 4, 6 i 8 cilindara.
'''
df2=pd.DataFrame({'v8': v8.wt, 'v6': v6.wt , 'b4':b4.wt}, columns=['v8', 'v6','b4'])

plt.figure()
df.plot(kind='box')
plt.title("distribuciju tezine automobila s 4, 6 i 8 cilindara")

'''
3. Pomocu odgovarajuceg grafa pokusajte odgovoriti na pitanje imaju li automobili s 
rucnim mjenjacem vecu potrosnju od automobila s automatskim mjenjacem?
'''
aut=mtcars[(mtcars.am == 0)]
#print aut
aut_br=len(aut.am)
#print'automobila sa automatskim mjenjacem ima: ' , aut_br

man=mtcars[(mtcars.am == 1)]
man_br=len(man.am)
#print'automobila sa rucnim mjenjacem ima: ' , man_br

df3=pd.DataFrame({'rucni':man.mpg , 'automatski': aut.mpg }, columns=['rucni', 'automatski'])
df3.plot(kind='bar')
plt.title('potrosnje automobila sa rucnim i automatskim mjenjacem')
'''
IMAJU 
'''
'''
4.Prikazite na istoj slici odnos ubrzanja i snage automobila za automobile s rucnim 
odnosno automatskim mjenjacem. 	qsec 	1/4 mile time 
'''


odnos_m=man.hp/man.qsec
print odnos_m

odnos_a=aut.hp/aut.qsec
#print odnos_a
#
#odnos_as=sort(odnos_a)
##print odnos_as
#odnos_ms=sort(odnos_m)
##print odnos_ms
#figure(4)
##plt.plot(odnos_as)
##plt.plot(odnos_ms)
#plt.title("odnos ubrzanja i snage")
#odnos_as.plot()
#odnos_ms.plot()

