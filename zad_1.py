# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 22:25:00 2015

@author: bec
"""
'''
Za mtcars skup podataka napišite programski kod koji ce odgovoriti na sljedeca pitanja:
1. Kojih 5 automobila ima najvecu potrosnju? (koristite funkciju sort)
2. Koja tri automobila s 8 cilindara imaju najmanju potrosnju?
3. Kolika je srednja potrosnja automobila sa 6 cilindara?
4. Kolika je srednja potrosnja automobila s 4 cilindra mase izmedju 2000 i 2200 lbs?
5. Koliko je automobila s rucnim, a koliko s automatskim mjenjacem u ovom skupu podataka?
6. Koliko je automobila s automatskim mjenjacem i snagom preko 100 konjskih snaga?
7. Kolika je masa svakog automobila u kilogramima?
'''

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
i=0
t=0
a=[]
br_hp=0

#za_ispis=np.array()
br=0
mtcars = pd.read_csv('mtcars.csv')
mpg_4_c=[]
mpg_4c=0
#data, index=index
#smpg=sorted(mtcars.mpg);
#print 'sortirani mpg',smpg;
#print mtcars[['car','mpg']]



'''
##1. Kojih 5 automobila ima najvecu potrosnju? (koristite funkciju sort)
'''
sortD=mtcars.sort(['mpg'])  # sortiranje po potrošnji 
print 'najvecih 5 potrosaca \n ', sortD.head (5).car #ispis po potrošnji
'''
##2. Koja tri automobila s 8 cilindara imaju najmanju potrosnju?
'''

v8=mtcars[(mtcars.cyl == 8)];
print'svi v8 nesortirano \n', v8
sortv8=v8.sort(['mpg']);  # sortiranje po potrošnji 

print'v8 modeli sa najmanjom potrosnjom\n', sortv8.tail(3).car # KASNIJE NAPRAVITI TAKO DA ISPISE I STUPAC MPG

'''
#3. Kolika je srednja potrosnja automobila sa 6 cilindara?
'''


v6=mtcars[(mtcars.cyl == 6)]
v6_mpg=mean(v6.mpg)
print 'srednja potrosnja automobila svih \n automobila sa 6 cilindara : ',v6_mpg

'''
#4. Kolika je srednja potrosnja automobila s 4 cilindra mase izmedju 2000 i 2200 lbs?
'''
#print mtcars.index
b4=mtcars[(mtcars.cyl == 4)]# sa 4 cilindra
#print b4.wt, b4.mpg
a=b4.index  #redni brojevi sa 4 cilindra, ovo je neka vrsta liste
for i in range (1,len(a)):
    #adresiranje pojedinih rednih brojeva koji su spremljeni u a, po njihovom indexu 
     #u listi a 
    if (b4.wt[a[i]]>=2.000) and (b4.wt[a[i]]<=2.20): #automobili izmedju 2000 i 2200 lbs
        #print br,'.','automobil ima masu: ',(b4.wt[a[i]])*1000, 'lbs'  
        
        mpg_4c=mpg_4c+b4.mpg[a[i]] #dodavanje vrijednosti mpg u jednu varijablu
        br=br+1             #brojanje koliko auta zadovoljava uvijet                                              
        
print 'prosjecna potrosnja ',mpg_4c/br 

    
'''
5. Koliko je automobila s rucnim, a koliko s automatskim mjenjacem u ovom skupu podataka?
'''


aut=mtcars[(mtcars.am == 0)]
#print aut
aut_br=len(aut.am)
#print'automobila sa automatskim mjenjacem ima: ' , aut_br

man=mtcars[(mtcars.am == 1)]
man_br=len(man.am)
#print'automobila sa rucnim mjenjacem ima: ' , man_br

'''
6. Koliko je automobila s automatskim mjenjacem i snagom preko 100 konjskih snaga?
'''
man_hp=man.hp
#print man_hp
man_in=0
man_in=man_hp.index
print man_in  
print 'duljina man_in', len(man_in)

for j in range(0,len(man_in)):
    if (man_hp[man_in[j]]>100):
        
        br_hp=br_hp+1
        
print 'broj automobila sa rucnimmjenjacem i snagom preko 100 konjskih snaga : ', br_hp


'''
7. Kolika je masa svakog automobila u kilogramima?
'''
m=mtcars.wt
for k in range (1,len(m)):
    m_kg=m[k]*0.45*1000
    print mtcars.wt[k]*0.45359237*1000, ' kg ',mtcars.car[k]
